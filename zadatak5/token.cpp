#include <iostream> 
#include <string> 
#include "token.h"
#include "functions.h" 

Token::Token( int i           /* =0   */, 
              std::string s   /* =""  */ 
              ) 
  : tag(i), lexeme(s)
{} 

Token& Token::operator=(Token const & t) { 
  tag = t.tag; 
  lexeme = t.lexeme; 
  return *this; 
} 

void Token::print() const { 
  std::cout << "<" << getTokenName(tag) << ", " << lexeme << "> " << std::endl ; 
} 
