Sadržaj
--------------

U repozitoriju se nalaze .cpp i .h fajlovi za implementaciju jednostavnog skenera koji vrši tokenizaciju ulaza prema primjeru sa predavanja P05.

Funkcija main() je u fajlu sc.cpp

Kompajliranje
---------------

1. Kompajliranje programa se vrši izvršavanjem komande make:
	$ make

Dobiće se izvršni fajl naziva scanner.

2. Brisanje fajlova generisanih prilikom kompajliranja vrši se komandom:
	$ make clean

3. Bezuslovno kompajliranje svih fajlova se vrši komandom:
	$ make rebuild
