#include <string>
#include "functions.h"
#include "token.h"
#include "inputbuffer.h"

Token getAssign(InputBuffer& b){
  b.startScan();
  char c ;
  c = b.getNext();
  if(c == '=') 
  {
    c = b.getNext();
    if(c == '=')
    {
      return Token(RELOP, b.getLexeme());
    }
    if(c) b.backtrack(1);
    return Token(ASSIGN, b.getLexeme());
  }
  return wrongToken(b);
}

Token getWs(InputBuffer & b){
  b.startScan();
  char c = b.getNext();
  if( c != ' ' && c != '\t' && c != '\n' )
    return wrongToken(b);

  while( (c = b.getNext()) && ( c == ' ' || c == '\t' || c == '\n' ));

  if(c) b.backtrack(1);
  return Token(WS);
}

Token getId(InputBuffer& b){
  b.startScan();
  char c = b.peekNext();  // lookahead
  if(!isalpha(c))         // regex [a-zA-Z]
    return wrongToken(b);

  while( (c = b.getNext()) && isalpha(c) ); // regex [a-zA-Z]*

  // vrati jedan karakter u buffer jer to nije slovo
  if(c) b.backtrack(1);
  return Token(ID, b.getLexeme());
}

Token getNum(InputBuffer& b){
  b.startScan();
  char c = b.getNext();
  if(!isdigit(c)) 
    return wrongToken(b);

  while( (c = b.getNext()) && isdigit(c) );

  if(c) b.backtrack(1);
  return Token(NUMBER, b.getLexeme());
}

Token getKeyword(InputBuffer& b){
  b.startScan();
  int state = 0;
  char c;
  do{
    c = b.peekNext();
    switch(state){
      case 0:
        if(c=='i')
          state = 1;
        else if(c=='f')
          state = 4;
        else if(c=='t')
          state = 8;
        else
          state = 99;
        break;
      case 1:
        if(c=='f')
          state = 2;
        else
          state = 99;
        break;
      case 2:
        if(isalpha(c))
          state = 99;
        else 
          return Token(IF, b.getLexeme());
        break;
      case 4:
        if(c=='o')
          state = 5;
        else
          state = 99;
        break;
      case 5:
        if(c=='r')
          state = 6;
        else
          state = 99;
        break;
      case 6:
        if(isalpha(c))
          state = 99;
        else 
          return Token(FOR, b.getLexeme());
        break;
      case 8:
        if(c=='h')
          state = 9;
        else
          state = 99;
        break;
      case 9:
        if(c=='e')
          state = 10;
        else
          state = 99;
        break;
      case 10:
        if(c=='n')
          state = 11;
        else
          state = 99;
        break;
      case 11:
        if(isalpha(c))
          state = 99;
        else {
          return Token(THEN, b.getLexeme());
        }
        break;
      case 99:
          return wrongToken(b);
    }
  } while( b.getNext() );

  return wrongToken(b);
}

std::string getTokenName(int tag){
  switch(tag){
    case IF:
      return "IF";
    case ID:
      return "ID";
    case FOR:
      return "FOR";
    case THEN:
      return "THEN";
    case ASSIGN:
      return "ASSIGN";
    case WS:
      return "WS";
    case RELOP:
      return "RELOP";
    case NUMBER:
      return "NUMBER";
    case 0:
      return "unknown";
    default:
      return std::string(1, tag)  ;
  }
}

Token wrongToken(InputBuffer & b){
  b.backtrack();
  return Token(0);
}
